import {GET_PHOTOS_PENDING, GET_PHOTOS_RESOLVE, GET_PHOTOS_ERROR} from '../constants';

const getPhotosPending = () => ({type: GET_PHOTOS_PENDING});
const getPhotosResolve = (payload) => ({type: GET_PHOTOS_RESOLVE, payload});
const getPhotosError = () => ({type: GET_PHOTOS_ERROR});

export const getPhotos = () => {
  return (dispatch) => {
    dispatch(getPhotosPending());
    fetch('https://jsonplaceholder.typicode.com/photos')
      .then((data) => data.json())
      .then((data) => dispatch(getPhotosResolve(data)))
      .catch(() => dispatch(getPhotosError()));
  };
};
