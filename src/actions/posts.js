import {GET_POSTS_PENDING, GET_POSTS_RESOLVE, GET_POSTS_ERROR} from '../constants';

const getPhotosPending = () => ({type: GET_POSTS_PENDING});
const getPhotosResolve = (payload) => ({type: GET_POSTS_RESOLVE, payload});
const getPhotosError = () => ({type: GET_POSTS_ERROR});

export const getPosts = () => {
  return (dispatch) => {
    dispatch(getPhotosPending());
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((data) => data.json())
      .then((data) => dispatch(getPhotosResolve(data)))
      .catch(() => dispatch(getPhotosError()));
  };
};
