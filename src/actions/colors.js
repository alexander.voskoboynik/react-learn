import {SET_COLOUR} from '../constants';

export const actionSetRed = () => ({type: SET_COLOUR, colour: 'red'});
export const actionSetGreen = () => ({type: SET_COLOUR, colour: 'green'});
export const actionSetBlue = () => ({type: SET_COLOUR, colour: 'blue'});
