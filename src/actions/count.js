import {COUNT_DECREMENT, COUNT_INCREMENT} from '../constants';

export const actionIncrement = () => ({type: COUNT_INCREMENT});
export const actionDecrement = () => ({type: COUNT_DECREMENT});
