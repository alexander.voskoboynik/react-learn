import React from 'react';
import {Routes, Route} from 'react-router-dom';
import LectureUsers from "./components/Users/lecture";
import UsersOl from "./components/Users/self/UsersOl";
import {PostsContainer} from "./components/Posts/Posts";
import {PhotoContainer} from './components/Photos/PhotoContainer';
import ButtonsContainer from './components/ButtonsContainer';
import {Form} from './components/Forms/Form';
import Context from './components/ContextApi';
import {Portal} from './components/Portal';
import Hooks from './components/Hooks';
import App from './App';

export const Main = () => {
  return (
    <>
      <h1>Header</h1>
      <Routes>
        <Route path="/" element={<App />} exact />
        <Route path="/posts" element={<PostsContainer />} />
        <Route path="/photos" element={<PhotoContainer />} />
        <Route path="/buttons" element={<ButtonsContainer />} />
        <Route path="/users/lecture/:userId" element={<LectureUsers />} />
        <Route path="/users/home" element={<UsersOl />} />
        <Route path="/form" element={<Form />} />
        <Route path="/context-api" element={<Context />} />
        <Route path="/portal" element={<Portal />} />
        <Route path="/hooks" element={<Hooks />} />
      </Routes>
    </>
  )
}
