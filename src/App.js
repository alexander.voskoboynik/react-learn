import {Link} from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import {MyComponents} from "./components/MyComponents";

function App() {
  const getData = () => {
    return 'Get Data';
  }
  return (
    <div className="App">
      <Link to="/photos">Photos</Link>
      <Link className="ml-8" to="/posts">Posts</Link>
      <Link className="ml-8" to="/buttons">Buttons</Link>
      <Link className="ml-8" to="/users/lecture/5">Lecture users</Link>
      <Link className="ml-8" to="/users/home">Users</Link>
      <Link className="ml-8" to="/form">Form</Link>
      <Link className="ml-8" to="/context-api">Context API</Link>
      <Link className="ml-8" to="/portal">Portal</Link>
      <Link className="ml-8" to="/hooks">Hooks</Link>
      <MyComponents.Header logo={logo}/>
      <MyComponents.Body />
      <MyComponents.Section
        text="Some text"
        isDefault={false}
        count={10}
        user={{name: 'John', age: 44}}
        data={getData()}
      />
    </div>
  );
}

export default App;
