import {COUNT_DECREMENT, COUNT_INCREMENT} from '../constants';

const initialState = {
  count: 0
};
export const countReducer = (state = initialState, action) => {
  switch(action.type) {
    case COUNT_INCREMENT: {
      return {
        count: state.count + 1
      };
    }
    case COUNT_DECREMENT: {
      return {
        count: state.count - 1
      };
    }
    default: {
      return state;
    }
  }
};
