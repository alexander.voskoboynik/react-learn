import {SET_COLOUR} from '../constants';

const initialState = {
  color: ''
};
export const colourReducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_COLOUR: {
      return {
        color: action.colour
      };
    }
    default: {
      return state;
    }
  }
};
