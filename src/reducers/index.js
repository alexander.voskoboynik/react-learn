import {combineReducers} from 'redux';
import {photoReducer} from './photo';
import {colourReducer} from './color';
import {countReducer} from './count';
import {postsReducer} from './posts';

export const rootReducer = combineReducers({
  postsState: postsReducer,
  photoState: photoReducer,
  countState: countReducer,
  colourState: colourReducer
});
