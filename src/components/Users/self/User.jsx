import React from 'react';

const User = ({user}) => (
  <li>
    <div>
      <p>fullName: {user.firstName} {user.lastName}</p>
      <span>firstName: {user.firstName}</span>
      <span className="ml-8">lastName: {user.lastName}</span>
      <span className="ml-8">age: {user.age + 5}</span>
      <span className="ml-8">job: {user.job}</span>
    </div>
  </li>
)
export default User;
