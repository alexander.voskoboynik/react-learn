import React from 'react';

const User = ({user}) => (
  <li>
    <div>
      <span>Name: {user.name}</span>
      <span>Age: {user.age}</span>
    </div>
  </li>
)
export default User;
