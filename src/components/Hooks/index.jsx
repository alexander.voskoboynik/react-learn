import React, {useState, useEffect} from 'react';
import PokemonProvider from './Context';
import Reference from './Reference';
import UserCounter from './UserHooks';

const Hooks= () => {
  const [count, setCount] = useState(0);
  const [color, setColor] = useState('red');
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    document.title = `You clicked ${count} times`;
  });
  // if useEffect has return it combine componentDidMount componentWillUnmount
  // componentDidMount
  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/posts?_page=2&_limit=10')
      .then((response) => {
        return response.json();
      })
      .then(data => setPosts(data))
  }, []);

  // componentDidUpdate
  useEffect(() => {
    console.log('update')
  });


  return (
    <>
      <div>
        <p>Clicked {count} times</p>
        <button className={color} onClick={() => {
          setCount(count + 1);
          setColor('red');
        }}>
          Increment
        </button>
        <button className={color} onClick={() => {
          setCount(count - 1);
          setColor('blue');
        }}>
          Decrement
        </button>
      </div>
      <div>
        {
          posts.map((item) => (
            <div className="post" key={item.id}>
              <p><strong>{item.title}</strong></p>
              <p>{item.body}</p>
            </div>
          ))
        }
      </div>
      <PokemonProvider />
      <Reference />
      <UserCounter />
    </>
  )
};

export default Hooks;
