import React, {useState, useRef} from 'react';

const Reference = () => {
  const [name, setName] = useState('Yoda')
  const nameInput = useRef();
  const submit = () => setName(nameInput.current.value);

  return (
    <div>
      <p>{name}</p>
      <div>
        <input ref={nameInput} type="text"/>
        <button type="button" onClick={submit}>Set name</button>
      </div>
    </div>
  );
};

export default Reference;
