import React from 'react';
import useCounter from '../../hooks/useCounter';


const UserCounter = () => {
  const [myCount, {increment, decrement}] = useCounter({initialState: 0})
  return (
    <div>
      <h2>{myCount}</h2>
      <button className="blue" onClick={increment}>Increment</button>
      <button className="red" onClick={decrement}>Decrement</button>
    </div>
  )
}

export default UserCounter;
