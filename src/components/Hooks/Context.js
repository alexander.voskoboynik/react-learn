import React, {useContext} from 'react';

const WhoThatPokemon = React.createContext();

const Pokemon = () => {
  const answer = useContext(WhoThatPokemon)
  return <p>{answer}</p>;
};

const PokemonProvider = () => {
  return (
    <WhoThatPokemon.Provider value={'Squirtle'}>
      <Pokemon />
    </WhoThatPokemon.Provider>
  );
};

export default PokemonProvider;
