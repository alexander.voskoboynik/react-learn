import React from 'react';
import ReactDOM from 'react-dom';
class Modal extends React.Component {
  state = {root: null};

  componentDidMount() {
    const root = document.createElement('div')
    document.body.append(root);

    this.setState({root})
  }
  componentWillUnmount() {
    this.state.root?.remove();
  }

  render() {
    return (
      this.state.root
        ? ReactDOM.createPortal(
          <div className="modal-wrapper">
            <div className="modal">
              <p>This is portal modal {this.props.text}</p>
              <button onClick={this.props.close}>Close</button>
            </div>
          </div>,
          this.state.root
        ) : null
    )
  }
}
export class Portal extends React.Component {
  state = {
    showModal: false
  }
  render() {
    return (
      <>
        <h2>Portal</h2>
        <button
          onClick={() => this.setState({showModal: true})}
        >
          Show Modal
        </button>
        {this.state.showModal && (
          <Modal
            text="Portal 3"
            close={() => this.setState({showModal: false})}
          />)}
      </>
    )
  }
}
