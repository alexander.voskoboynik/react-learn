import React from 'react';

export class Form extends React.Component {
  state =  {
    value: 'hello',
    textAreaValue: '',
    checkValue: false,
    selected: 2,
    options: ['HTML', 'CSS', 'Javascript'],
    radioOption: ''
  }
  handleChangeInput = (event) => {this.setState({value: event.target.value})};
  handleChangeArea = (event) => {this.setState({textAreaValue: event.target.value})};
  handleChangeCheckbox = () => {this.setState({checked: !this.state.checkValue})};
  handleChangeSelect = (event) => {this.setState({selected:  event.target.value})};
  handleChangeRadio = (event) => {this.setState({radioOption: event.target.value})};

  render() {
    return (
      <div>
        <input
          value={this.state.value}
          onChange={this.handleChangeInput}
        />
        <hr/>
        <textarea
          cols="40"
          rows="3"
          value={this.state.textAreaValue}
          onChange={this.handleChangeArea}
        />
        <hr/>
        <input type="checkbox" checked={this.checkValue} onChange={this.handleChangeCheckbox}/>
        <hr/>
        <p>Selected : {this.state.options[this.state.selected]}</p>
        <select value={this.state.selected} onChange={this.handleChangeSelect}>
          {
            this.state.options.map((item, index) => (
              <option key={index} value={index}>{item}</option>
            ))
          }
        </select>
        <hr/>
        <p>Selected Radio : {this.state.radioOption}</p>
        <input
          type="radio"
          value="value1"
          checked={this.state.radioOption === 'value1'}
          onChange={this.handleChangeRadio}
        />
        <input
          type="radio"
          value="value2"
          checked={this.state.radioOption === 'value2'}
          onChange={this.handleChangeRadio}
        />
      </div>
    )
  }
}
