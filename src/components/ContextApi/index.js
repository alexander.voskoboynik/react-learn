import React from 'react';

const MyContext = React.createContext();
const Input = ({value, handleChange}) => (
  <input
    type="text"
    value={value}
    onChange={handleChange}
  />
)
const User = () => (
  <MyContext.Consumer>
    {Input}
  </MyContext.Consumer>
)
const Card = () => (
  <User />
)
const Column = () => (
  <Card />
);
class Context extends React.Component {
  state = {
    value: ''
  }
  handleChange = (event) => {
    this.setState({value: event.target.value});
  }
  render() {
    return (
      <div>
        <h1>Context API</h1>
        <MyContext.Provider value={{
          value: this.state.value,
          handleChange: this.handleChange
        }}>
          <Column />
        </MyContext.Provider>
      </div>
    );
  }
}

export default Context
