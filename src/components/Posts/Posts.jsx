import React from 'react';
import {connect} from 'react-redux';
import {getPosts} from '../../actions/posts';

class Posts extends React.Component {

  componentDidMount() {
    this.props.getPosts();
  }
  render() {
    const {isLoading, posts} = this.props;
    return (
      <div>
        {
          isLoading ? (
              <h2>Is loading...</h2>
            ) : (
              <div>
                {
                  posts.map((item) => (
                    <div className="post" key={item.id}>
                      <div>{item.id}</div>
                      <div><strong>Title: </strong>{item.title}</div>
                      <div><strong>Description: </strong>{item.body}</div>
                    </div>
                  ))
                }
              </div>
            )
        }
      </div>
    )
  };
}

const mapStateToProps = (state) => ({
  isLoading: state.postsState.isLoading,
  posts: state.postsState.posts
});
const mapDispatchToProps = {
  getPosts
};
export const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(Posts);
