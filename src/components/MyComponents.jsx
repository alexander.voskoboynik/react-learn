import User from './Users/lecture/User'

export const MyComponents = {
  Header: ({logo, ...others}) => {
    return (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    )
  },

  Body: () => <h1>This is Body</h1>,
  Section: (props) => {
    return (
      <section>
        <p>{props.text}</p>
        {
          props.isDefault
            ? (<div>This is default</div>)
            : (<div>This is not default</div>)
        }
        {
          !props.isDefault && <h3>Hi</h3>
        }
        <span>{props.count}</span>
        <div>
          <h3>Info about user</h3>
          <p>Name: {props.user.name}</p>
          <p>Age: {props.user.age}</p>
        </div>
        <div>{props.data}</div>
      </section>
    )
  },
  Users: () => {
    const users = [
      {id: 1, name: 'John', age: 33},
      {id: 2, name: 'Kate', age: 31},
      {id: 3, name: 'Bob', age: 13}
    ]
    return (
      <>
        <h3>Info about users</h3>
        <ul>
          {
            users.map((item) => <User key={item.id} user={item} />)
          }
        </ul>
      </>
    )
  }
};
