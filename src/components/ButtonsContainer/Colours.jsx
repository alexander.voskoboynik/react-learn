import React from 'react';
import {connect} from 'react-redux';
import {actionSetGreen, actionSetBlue, actionSetRed} from '../../actions/colors';

const Buttons = ({color, actionSetBlue, actionSetGreen, actionSetRed}) => {
  return (
    <div>
      <button className={color} onClick={actionSetRed}>Set Red</button>
      <button className={color} onClick={actionSetGreen}>Set Green</button>
      <button className={color} onClick={actionSetBlue}>Set Blue</button>
    </div>
  )
}

const mapStateToProps = (state) => ({
  color: state.colourState.color
});

const mapDispatchToProps = {
  actionSetBlue, actionSetGreen, actionSetRed
};

export const ColourButtons = connect(mapStateToProps, mapDispatchToProps)(Buttons);

