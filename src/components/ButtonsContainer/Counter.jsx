import React from 'react';
import {connect} from 'react-redux';
import {actionIncrement, actionDecrement} from '../../actions/count';

const Counter = ({number, actionIncrement, actionDec}) => {
  return (
    <div>
      <button onClick={actionIncrement}>Increment</button>
      <span>{number}</span>
      <button onClick={actionDec}>Decrement</button>
    </div>
  )
}

const mapStateToProps = (state) => ({
  number: state.countState.count
});

const mapDispatchToProps = {
  actionIncrement,
  actionDec: actionDecrement
};

export const CountContainer = connect(mapStateToProps, mapDispatchToProps)(Counter);

