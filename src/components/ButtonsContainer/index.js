import React from 'react';
import {ColourButtons} from "./Colours";
import {CountContainer} from "./Counter";

const ButtonsContainer = ({color, actionSetBlue, actionSetGreen, actionSetRed}) => {
  return (
    <div>
      <ColourButtons />
      <CountContainer />
    </div>
  )
}

export default ButtonsContainer;
